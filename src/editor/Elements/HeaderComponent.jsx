import React, {useState} from 'react';

const HeaderComponent = (props) => {

    const  styleHeaderComponent = {
        height: "50px",
        outline: "1px dotted grey",
        zIndex: "1000",
        textAlign: "center",
        justifyContent: "center",
        marginLeft: props.pageMarginsLeft,
        marginRight: props.pageMarginsRight,
    };

    return (
        <div style={styleHeaderComponent}>
            <h6>Верхний колонтитул</h6>
        </div>
    );
};

export default HeaderComponent;