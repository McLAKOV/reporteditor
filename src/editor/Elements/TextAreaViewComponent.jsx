import React from 'react';

const TextAreaViewComponent = (props) => {

    const styleComponent = {
        font: props.templateProp.formatData.font,
        fontSize: props.templateProp.formatData.fontSize,
    }

    return (
        <div>
            <input
                style={{width: "100%", height: "100%", border: "none"}}
                id={props.id}
                onBlur={props.setHInArr}
                defaultValue={props.templateProp.data}
                ref={props.refH}
            />
        </div>

    );
};

export default TextAreaViewComponent;