import React from 'react';

const BreakViewComponent = (props) => {
    return (
        <div>
            <i className="fas fa-page-break">Разрыв</i>
        </div>
    );
};

export default BreakViewComponent;