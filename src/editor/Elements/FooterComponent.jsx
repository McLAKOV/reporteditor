import React from 'react';
import logo from '../../pic.jpg';

const FooterComponent = (props) => {

    const  styleFooterComponent = {
        height: "80px",
        width: props.pageMarginsSizeWidth,
        outline: "1px dotted grey",
        zIndex: "1000",
        textAlign: "center",
        justifyContent: "center",
        marginLeft: props.pageMarginsLeft,
        marginRight: props.pageMarginsRight,
        bottom: "0px",
        position: "absolute",
    };

    return (
        <div style={styleFooterComponent}>
            <img src={logo}  style={{ bottom: "0px", left: "0px",
                position: "absolute",}}/>
            <h6>Нижний колонтитул</h6>
        </div>
    );
};

export default FooterComponent;