import React, {useState} from 'react';


const Button = (props) => {

    function buttonClickHandler(name) {
          props.onButtonClick(name)
    };

    const styleButton = {
        margin: "5px 5px 5px 5px",
    };

    let btn = (
        props.arrElemDefault.map((elem)=>{
            return (
                <button
                    onClick={()=> buttonClickHandler(elem.viewComponent.name)}
                    style={styleButton}
                    className="btn btn-primary"
                >{elem.viewComponent.name}</button>
            )
        })
    )

    return (
        <div>
            {btn}
        </div>
    );
};

export default Button;