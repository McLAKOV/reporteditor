import React from 'react';
import "../fontawesome-pro-5.12.0-web/css/all.css";

const ResizeMoveWrapper = (WrappedComponent) => {
    let  xDif, yDif;

    const ComponentWithResizeMoveWrapper = (props)=>{//Higher-Order Component

        const  styleComponent = {
            top: props.editorProp.position.top,
            left: props.editorProp.position.left,
            height: props.editorProp.size.height,
            width: props.editorProp.size.width,
            border: "2px solid gray",
            zIndex: "1000",
            textAlign: "center",
            justifyContent: "center",
            position: "absolute",
            visibility: `${props.editorProp.select ? "hidden" : ""}`,
        };

        const styleCircle1 = {
            top: "-9px",
            left: eval((props.editorProp.size.width).slice(0, -2))/2-8 + "px",
            color: "red",
            position: "absolute",
            visibility: `${props.editorProp.select ? "" : "hidden"}`,
        };

        const styleCircle2 = {
            top: eval((props.editorProp.size.height).slice(0, -2))/2-9 + "px",
            left: "-9px",
            color: "red",
            position: "absolute",
            visibility: `${props.editorProp.select ? "" : "hidden"}`,
        };

        const styleCircle3 = {
            top: eval((props.editorProp.size.height).slice(0, -2))/2-9 + "px",
            // left: eval((props.editorProp.size.width).slice(0, -2))-7 + "px",
            left: eval((props.editorProp.size.width).slice(0, -2))-11 + "px",
            color: "red",
            position: "absolute",
            visibility: `${props.editorProp.select ? "" : "hidden"}`,
        };

        const styleCircle4 = {
            // top: eval((props.editorProp.size.height).slice(0, -2))-7 + "px",
            top: eval((props.editorProp.size.height).slice(0, -2))-11 + "px",
            left: eval((props.editorProp.size.width).slice(0, -2))/2-8 + "px",
            color: "red",
            position: "absolute",
            visibility: `${props.editorProp.select ? "" : "hidden"}`,
        };

        const styleHeader = {
            backgroundColor: "gray",
            top: (props.editorProp.position.top).slice(0, -2)-20 + "px",
            left: props.editorProp.position.left,
            height: "20px",
            // width: eval((props.editorProp.size.width).slice(0, -2))+4 + "px",
            width: eval((props.editorProp.size.width).slice(0, -2)) + "px",
            zIndex: "1000",
            textAlign: "center",
            justifyContent: "center",
            position: "absolute",
            cursor: "move",
        };

        const styleButton = {
            float: "right",
            height: "18px",
            width: "18px",
            margin: "2px",
            textAlign: "center",
            justifyContent: "center",
            cursor: "default",
        };

        function dragStartHandler (e) {
            xDif = e.pageX - props.editorProp.position.left.slice(0, -2);
            yDif = props.editorProp.position.top.slice(0, -2) - e.pageY;
        };

        function dragEndHandler (e) {
            e.preventDefault();
            let prop = "position";
            let xEnd = (e.pageX - xDif) + "px";
            let yEnd = (e.pageY + yDif) + "px";
            props.setProp(props.id, prop, xEnd, yEnd);
        };


        // function mouseDownHandler(e) {
        //     console.log("mouseDownHandler");
        //     flag = true;
        //     // setHidElem("hidden");
        //     // props.paintElem(props.editorProp.position.top, props.editorProp.position.left, props.editorProp.size.height, props.editorProp.size.width);
        //
        // };
        //
        // function mouseUpHandler(e) {
        //     console.log("mouseUpHandler");
        //     // if (flag) {
        //     //     let prop = "size";
        //     //     let xEnd = (e.pageX - props.editorProp.position.left.slice(0, -2)) + "px";
        //     //     let yEnd = (e.pageY - props.editorProp.position.top.slice(0, -2)) + "px";
        //     //     console.log('xEnd', xEnd);
        //     //     console.log('yEnd', yEnd);
        //     //     props.setProp(props.id, prop, xEnd, yEnd);
        //     // }
        //     flag = false;
        //
        //     // setHidElem("");
        // };
        //
        // function mouseMoveHandler(e) {
        //     console.log("mouseMoveHandler");
        //     console.log("flag", flag)
        //     if (flag) {
        //         let prop = "size";
        //         let xEnd = (e.pageX - props.editorProp.position.left.slice(0, -2)) + "px";
        //         let yEnd = (e.pageY - props.editorProp.position.top.slice(0, -2)) + "px";
        //         console.log('xEnd', xEnd);
        //         console.log('yEnd', yEnd);
        //         props.setProp(props.id, prop, xEnd, yEnd);
        //     }
        //     // flag = false;
        // };

        return (
            <div
                id={props.id}
                onDragStart={(e) => dragStartHandler (e)}
                onDragEnd={(e) => dragEndHandler (e)}
            >
                <div
                    draggable={true}
                    className="headerComponent"
                    id={props.id}
                    style={styleHeader}
                    // style={{display: "none"}}//удалить
                    onMouseUp={(e) => props.selectElem (props.id)}
                >
                    <i
                        id={props.id}
                        style={styleButton}
                        onClick={(e) => props.delElem (props.id)}
                        className="far fa-window-close"></i>
                </div>

                <div
                    className="component"
                    id={props.id}
                    // style={props.editorProp.select ? styleSelectComponent : styleComponent}
                    style={styleComponent}
                    // style={{display: "none"}}//удалить
                >

                    {/*<WrappedComponent {props.editorProp,...props}/>*/}
                    {/*<WrappedComponent {...props}/>//все пропсы*/}
                    <WrappedComponent
                        id={props.id}
                        templateProp = {props.templateProp}
                        handleChangeH = {props.handleChangeH}
                        setHInArr = {props.setHInArr}
                        refH={props.refH}
                    />
                    {/*<i*/}
                    {/*    style={styleCircle1}*/}

                    {/*    className="fas fa-circle"></i>*/}
                    {/*<i*/}
                    {/*    style={styleCircle2}*/}
                    {/*    className="fas fa-circle"></i>*/}
                    {/*<i*/}
                    {/*    onClick={(e) => props.reSize(props.id)}*/}
                    {/*    style={styleCircle3}*/}
                    {/*    className="fas fa-circle"></i>*/}
                    {/*<i*/}
                    {/*    style={styleCircle4}*/}
                    {/*    className="fas fa-circle"></i>*/}
                    {/*<i*/}
                    {/*    className="fal fa-arrows"*/}
                    {/*    style={styleArrow}*/}
                    {/*// onMouseDown={(e) => mouseDownHandler(e)}*/}
                    {/*></i>*/}
                </div>
            </div>
        );
    };

    return ComponentWithResizeMoveWrapper;
};

export default ResizeMoveWrapper;