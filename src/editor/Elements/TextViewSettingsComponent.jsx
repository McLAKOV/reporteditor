import React from 'react';

const TextViewSettingsComponent = (props) => {

    return (
        <div className="textViewSettingsComponent">
            <h4>Text settings</h4>
            <table>
                <tr>
                    <td>FontSize</td>
                    <td><input
                        id="fontSize"
                        onChange={props.handleChange}
                        defaultValue={(props.templateProp.formatData.fontSize).slice(0, -2)}/></td>
                </tr>
            </table>
        </div>
    );
};

export default TextViewSettingsComponent;