import React from 'react';
import PageSettingsComponent from "./Elements/PageSettingsComponent";

const EditorViewSettingsSidebarComponent = (props) => {
    const  styleSett = {
        height: "1000px",
        zIndex: "1000",
        justifyContent: "center",
        float: "left",
        marginLeft: "30px",
        marginTop: "30px",
    };
    let ViewSettingsComponent = props.viewSettingsComponent;
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//     const EnhancedComponent = TemplatePropWrapper (props.viewSettingsComponent)
//     const ViewSettingsComponent = props.viewSettingsComponent;
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    function handleChange(e) {
        props.setSettings( e.target.value, e.target.id);
    };

    return (
        <div
            className="settings"
            className="6"
            style={styleSett}>
            <PageSettingsComponent
                pageMarginsLeft={props.pageMarginsLeft}
                pageMarginsTop={props.pageMarginsTop}
                pageMarginsRight={props.pageMarginsRight}
                pageMarginsBottom={props.pageMarginsBottom}
                reSizePageMarginsHandleChange={props.reSizePageMarginsHandleChange}
                reOrientation={props.reOrientation}
            />

            <h4>Settings</h4>
            <table >
                <tr>
                    <td>ID</td>
                    <td><input
                        id = "ID"
                        onChange={handleChange}
                        defaultValue={props.id}/></td>
                </tr>
                <tr>
                    <td>Width</td>
                    <td><input
                        id = "width"
                        onChange={handleChange}
                        defaultValue={props.width}/></td>
                </tr>
                <tr>
                    <td>Height</td>
                    <td><input
                        id = "height"
                        onChange={handleChange}
                        defaultValue={props.height}/></td>
                </tr>
            </table>
            <ViewSettingsComponent
                templateProp={props.templateProp}
                handleChange={handleChange}
            />
        </div>
    );
};

export default EditorViewSettingsSidebarComponent;