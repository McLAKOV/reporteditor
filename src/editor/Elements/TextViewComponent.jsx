import React from 'react';

const TextViewComponent = (props) => {

    const styleComponent = {
        font: props.templateProp.formatData.font,
        fontSize: props.templateProp.formatData.fontSize,
    }

    return (
        <span style={styleComponent}>{props.templateProp.data}</span>

    );
};

export default TextViewComponent;