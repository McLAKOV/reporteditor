import React, {useState} from 'react';
import Button from "./Elements/Button";

const EditorToolbarComponent = (props) => {

    let pageMargins = [props.pageMarginsLeft, props.pageMarginsTop, props.pageMarginsRight, props.pageMarginsBottom];

    const  styleComponent = {
        height: "50px",
        border: "2px solid red",
        zIndex: "1000",
        textAlign: "center",
        justifyContent: "center",
    };

    return (
        <div
            style={styleComponent}
        >
            <Button
                arrElemDefault={props.arrElemDefault}
                onButtonClick={props.onButtonClick}
            />
            <button
                disabled={!(props.select)}
                className="btn btn-danger"
                onClick={() => props.delElem(props.id)}
            >DEL</button>
            <button
                className="btn btn-primary"
                onClick={() => {props.createReport();
                props.copyParamToPdf(props.arrElem, props.pageOrientation, props.pageSize, pageMargins);
                }}>
                Create report
            </button>
            <button
                className="btn btn-primary"
                onClick={() =>
                    props.copyParamToPdf(props.arrElem, props.pageOrientation, props.pageSize, pageMargins)
                }>
               Copy params
            </button>
        </div>
    );
};

export default EditorToolbarComponent;