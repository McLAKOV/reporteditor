import React from 'react';

const PageSettingsComponent = (props) => {

    return (
        <div>
            <h4>Page margins</h4>
            <table>
                <tr>
                    <td>Left</td>
                    <td><input
                        id = "pageMarginsLeft"
                        type="number"
                        onChange={props.reSizePageMarginsHandleChange}
                        defaultValue={(props.pageMarginsLeft).slice(0, -2)}
                    /></td>
                </tr>
                <tr>
                    <td>Top</td>
                    <td><input
                        id = "pageMarginsTop"
                        type="number"
                        onChange={props.reSizePageMarginsHandleChange}
                        defaultValue={(props.pageMarginsTop).slice(0, -2)}
                    /></td>
                </tr>
                <tr>
                    <td>Right</td>
                    <td><input
                        id = "pageMarginsRight"
                        type="number"
                        onChange={props.reSizePageMarginsHandleChange}
                        defaultValue={(props.pageMarginsRight).slice(0, -2)}
                    /></td>
                </tr>
                <tr>
                    <td>Bottom</td>
                    <td><input
                        id = "pageMarginsBottom"
                        type="number"
                        onChange={props.reSizePageMarginsHandleChange}
                        defaultValue={(props.pageMarginsBottom).slice(0, -2)}
                    /></td>
                </tr>
            </table>
            <div>
                <h4>Page orientation</h4>
                <div className="form-check">
                    <input className="form-check-input" type="radio" name="flexRadioDefault" value={"portrait"}
                           onChange={props.reOrientation}
                           id="flexRadioDefault2"
                           defaultChecked
                    />
                    <label className="form-check-label" htmlFor="flexRadioDefault2">
                        Portrait
                    </label>
                </div>
                <div className="form-check">
                    <input className="form-check-input" type="radio" name="flexRadioDefault" value={"landscape"}
                           onChange={props.reOrientation}
                           id="flexRadioDefault1"
                    />
                    <label className="form-check-label" htmlFor="flexRadioDefault1">
                        Landscape
                    </label>
                </div>
                <div>
                    <h4>Page size</h4>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="flexRadioDefault1" value={"A4"}
                               onChange={props.reOrientation}
                               id="flexRadioDefault1"
                               defaultChecked
                        />
                        <label className="form-check-label" htmlFor="flexRadioDefault3">
                            A4
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="flexRadioDefault1" value={"A3"}
                               onChange={props.reOrientation}
                               id="flexRadioDefault2"
                        />
                        <label className="form-check-label" htmlFor="flexRadioDefault4">
                            A3
                        </label>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PageSettingsComponent;