import React from 'react';

const TextAreaViewSettingsComponent = (props) => {

    return (
        <div className="textAreaViewSettingsComponent">
            <h4>Textarea settings</h4>
            <table>
                <tr>
                    <td>FontSize</td>
                    <td><input
                        id="fontSize"
                        onChange={props.handleChange}
                        defaultValue={(props.templateProp.formatData.fontSize).slice(0, -2)}/></td>
                </tr>
            </table>
        </div>
    );
};

export default TextAreaViewSettingsComponent;