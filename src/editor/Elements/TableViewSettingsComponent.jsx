import React from 'react';

const TableViewSettingsComponent = (props) => {

    return (
        <div
            className="tableViewSettingsComponent">
            <h4>Table settings</h4>
            <table>
                <tr>
                    <td>FontSize</td>
                    <td><input
                        id="fontSize"
                        onChange={props.handleChange}
                        defaultValue={(props.templateProp.formatData.fontSize).slice(0, -2)}/></td>

                </tr>
                <tr>
                    <td>Columns</td>
                    <td>
                        <input
                            id="columns"
                            defaultValue={(props.templateProp.formatData.columns)}/></td>
                </tr>
            </table>
        </div>
    );
};

export default TableViewSettingsComponent;