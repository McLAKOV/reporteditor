import React from 'react';

const TableViewComponent = (props) => {
    const styleComponent = {
        font: props.templateProp.formatData.font,
        fontSize: props.templateProp.formatData.fontSize,
    };



    let dataTable = props.templateProp.data;
    let tHeadArr = props.templateProp.signalArr;
    let tHead = tHeadArr.map((item) =>
        (
            <th>{item}</th>
        ));


    let arr = dataTable.map((item, index) => {
        let arrValue = [];
        for (let key in item) {
            arrValue.push(item[key]);
        };
        arrValue = arrValue.map ((i)=>
                (<td>{i}</td>)
            )
        return (
            <tr key={index}>
                {arrValue}
            </tr>
        )
        }
        );


    return (
            <table style={styleComponent}>
                <tr>
                    {tHead}
                </tr>
                {arr}
            </table>
    );
};

export default TableViewComponent;