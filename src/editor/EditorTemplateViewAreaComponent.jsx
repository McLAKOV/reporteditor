import React from 'react';
import {useState, useEffect, useRef} from 'react';
import { nanoid } from 'nanoid';
import TableViewComponent from "./Elements/TableViewComponent";
import TextViewComponent from "./Elements/TextViewComponent";
import TextAreaViewComponent from "./Elements/TextAreaViewComponent";
import ResizeMoveWrapper from "./ResizeMoveWrapper";
import "../fontawesome-pro-5.12.0-web/css/all.css";
import EditorViewSettingsSidebarComponent from "./EditorViewSettingsSidebarComponent";
import BreakViewComponent from "./Elements/BreakViewComponent";
import EditorToolbarComponent from "./EditorToolbarComponent";
import TextViewSettingsComponent from "./Elements/TextViewSettingsComponent";
import TextAreaViewSettingsComponent from "./Elements/TextAreaViewSettingsComponent";
import TableViewSettingsComponent from "./Elements/TableViewSettingsComponent";
import SettingsComponent from "./Elements/SettingsComponent";
import { cloneDeep, find, extend } from 'lodash';
import HeaderComponent from "./Elements/HeaderComponent";
import FooterComponent from "./Elements/FooterComponent";


const EditorTemplateViewAreaComponent = (props) => {

    //параметры страницы
    let [pageSizeHeight, setPageSizeHeight] = useState("1119px");
    let [pageSizeWidth, setPageSizeWidth] = useState("790px");
    //Page margins
    //left
    let [pageMarginsLeft, setPageMarginsLeft] = useState("0px");
    //top
    let [pageMarginsTop, setPageMarginsTop] = useState("0px");
    //right
    let [pageMarginsRight, setPageMarginsRight] = useState("0px");
    //bottom
    let [pageMarginsBottom, setPageMarginsBottom] = useState("0px");
    // //Page orientation
    let [pageOrientation, setPageOrientation] = useState("portrait");
    // //Page size
    let [pageSize, setPageSize] = useState("A4");
    let [pageMarginsSizeHeight, setPageMarginsSizeHeight] = useState("989px");//"1119px до колонтитулов в а4
    let [pageMarginsSizeWidth, setPageMarginsSizeWidth] = useState("790px");



    //массив отображаемых элементов которые отрисованы при запуске
    const [arrElem, setArrElem] = useState([
        // {id: 111,
        //     viewComponent: TextViewComponent,
        //     viewSettingsComponentName: "textViewSettingsComponent",
        //     templateProp: {
        //         data: "111",
        //         formatData: {
        //             a: 1,
        //             font: "",
        //             fontSize: "11px",
        //         },
        //     },
        //     editorProp: {
        //         position: {
        //             top: "100px",
        //             left: "100px",
        //         },
        //         size: {
        //             width: "300px",
        //             height: "100px",
        //         },
        //         select: false,
        //     },
        // },
        {id: 222,
            viewComponent: TextAreaViewComponent,
            viewSettingsComponentName: "textAreaViewSettingsComponent",
            templateProp: {
                data: "Надпись 1",
                formatData: {
                    a: 2,
                    font: "",
                    fontSize: "22px",
                },
            },
            editorProp: {
                position: {
                    top: "100px",
                    left: "100px",
                },
                size: {
                    width: "300px",
                    height: "34px",
                },
                select: false,
            },
        },
        {id: 333,
            viewComponent: TableViewComponent,
            viewSettingsComponent: TableViewSettingsComponent,
            viewSettingsComponentName: "tableViewSettingsComponent",
            templateProp: {
                signalArr: ["dateTime", "40SER11CP004", "40SER11CP005", "40SER11CP006", "40SER11CP007", "40SER11CP008", "40SER11CP009", "40SER11CP003", "40SER11CP002"],
                data: [
                    {
                        dateTime: "2019-09-14 12:21:08.392",
                        "SER11CP004": "0,11",
                        "SER11CP005": "0,18",
                        "SER11CP006": "0,24",
                        "SER11CP007": "0,66",
                        "SER11CP008": "0,33",
                        "SER11CP009": "0,08",
                        "SER11CP003": "0,11",
                        "SER11CP002": "0,18",
                    },
                    {
                        dateTime: "2019-09-14 13:21:08.392",
                        "SER11CP004": "0,11",
                        "SER11CP005": "0,18",
                        "SER11CP006": "0,24",
                        "SER11CP007": "0,66",
                        "SER11CP008": "0,33",
                        "SER11CP009": "0,08",
                        "SER11CP003": "0,11",
                        "SER11CP002": "0,18",
                    },
                    {
                        dateTime: "2019-09-14 14:21:08.392",
                        "SER11CP004": "0,11",
                        "SER11CP005": "0,18",
                        "SER11CP006": "0,24",
                        "SER11CP007": "0,66",
                        "SER11CP008": "0,33",
                        "SER11CP009": "0,08",
                        "SER11CP003": "0,11",
                        "SER11CP002": "0,18",
                    },
                    {
                        dateTime: "2019-09-14 15:21:08.392",
                        "SER11CP004": "0,11",
                        "SER11CP005": "0,18",
                        "SER11CP006": "0,24",
                        "SER11CP007": "0,66",
                        "SER11CP008": "0,33",
                        "SER11CP009": "0,08",
                        "SER11CP003": "0,11",
                        "SER11CP002": "0,18",
                    },
                ],

                formatData: {
                    a: 3,
                    font: "",
                    fontSize: "10px",
                    columns: 3,
                },
            },
            editorProp: {
                position: {
                    top: "300px",
                    left: "100px",
                },
                size: {
                    width: "650px",
                    height: "150px",
                },
                select: false,
            },
        },
        {id: 444,
            viewComponent: BreakViewComponent,
            viewSettingsComponentName: "textAreaViewSettingsComponent",
            templateProp: {
                data: "Надпись 1",
                formatData: {
                    a: 2,
                    font: "",
                    fontSize: "22px",
                },
            },
            editorProp: {
                position: {
                    top: "200px",
                    left: "200px",
                },
                size: {
                    width: "186px",
                    height: "34px",
                },
                select: false,
            },
        },
    ]);


    function reOrientation(e) {
        console.log("e.target.value", e.target.value)
        if ((e.target.value == "portrait" && pageSize == "A4") || (e.target.value == "A4" && pageOrientation == "portrait")) {
            setPageSizeHeight("1119px");
            setPageSizeWidth("790px");
            setPageMarginsSizeHeight("989px");
            setPageMarginsSizeWidth("790px");
            setPageOrientation("portrait");
            setPageSize("A4");
        }
        if ((e.target.value == "portrait" && pageSize == "A3") || (e.target.value == "A3" && pageOrientation == "portrait")) {
            setPageSizeHeight("2238px");
            setPageSizeWidth("1580px");
            setPageMarginsSizeHeight("2108px");//2238
            setPageMarginsSizeWidth("1580px");
            setPageOrientation("portrait");
            setPageSize("A3");
        }
        if ((e.target.value == "landscape" && pageSize == "A4") || (e.target.value == "A4" && pageOrientation == "landscape")) {
            setPageSizeHeight("790px");
            setPageSizeWidth("1119px");
            setPageMarginsSizeHeight("660px");
            setPageMarginsSizeWidth("1119px");
            setPageOrientation("landscape");
            setPageSize("A4");
        }
        if ((e.target.value == "landscape" && pageSize == "A3") || (e.target.value == "A3" && pageOrientation == "landscape")) {
            setPageSizeHeight("1580px");
            setPageSizeWidth("2238px");
            setPageMarginsSizeHeight("1450px");//1580
            setPageMarginsSizeWidth("2238px");
            setPageOrientation("landscape");
            setPageSize("A3");
        }
        setPageMarginsLeft("0px");
        setPageMarginsTop("0px");
        setPageMarginsRight("0px");
        setPageMarginsBottom("0px");
    }

    function reSizePageMarginsHandleChange(e){
        if(e.target.id == "pageMarginsLeft"){
            setPageMarginsLeft(e.target.value+"px");
            setPageMarginsSizeWidth((pageSizeWidth).slice(0, -2) - e.target.value - (pageMarginsRight).slice(0, -2) + "px");
        }
        if(e.target.id == "pageMarginsRight"){
            setPageMarginsRight(e.target.value+"px");
            setPageMarginsSizeWidth((pageSizeWidth).slice(0, -2) - e.target.value - (pageMarginsLeft).slice(0, -2) + "px");
        }
        if(e.target.id == "pageMarginsTop"){
            setPageMarginsTop(e.target.value+"px");
            setPageMarginsSizeHeight((pageSizeHeight).slice(0, -2) - e.target.value-130 - (pageMarginsBottom).slice(0, -2) + "px");
        }
        if(e.target.id == "pageMarginsBottom"){
            setPageMarginsBottom(e.target.value+"px");
            setPageMarginsSizeHeight((pageSizeHeight).slice(0, -2) - e.target.value-130 - (pageMarginsTop).slice(0, -2) + "px"); //130 размер колонтитулов
        }
    };


    const refH = useRef('Заголовок 1');
    let [top, setTop] = useState();
    let [left, setLeft] = useState();
    let [height, setHeight] = useState();
    let [width, setWidth] = useState();
    let [id, setId] = useState();
    let [hid, setHid] = useState("hidden");
    let [addComponent, setAddComponent] = useState();
    let [flagAdd, setFlagAdd] = useState(false);
    let [flagAdd2, setFlagAdd2] = useState(false);
    let [select, setSelect] = useState(false);
    let [viewSettingsComponent, setViewSettingsComponent] = useState();
    let [templateProp, setTemplateProp] = useState({
        data: "111",
        formatData: {
            a: 1,
            font: "",
            fontSize: "11px",
        },
    });

    function handleChangeH (e){
        console.log("refH", refH.current.value)
        console.log(e.target.value);
        };

    function setHInArr (e) {
        let id = eval(e.target.parentNode.parentNode.id);
        let index = getPos(id);
        console.log("index", index)
        let tempArr = [...arrElem];
        tempArr[index].templateProp.data = refH.current.value;
        setArrElem([...tempArr]);
    };

    function mouseDownHandlerNewElement (e) {

    };

    function mouseMoveHandlerNewElement (e) {
        if(flagAdd) {
            setTop(e.pageY-100);
            setLeft(e.pageX-550);
            setWidth (300);
            setHeight (100);
        }
    };

    function mouseUpHandlerNewElement (e) {
        if(flagAdd) {
            let tempArr = [...arrElem];
            let arrElemDefault= [...props.arrElemDefault];
            let newAddComponent = {};
            arrElemDefault.map((elem,i)=> {
                    if (elem.viewComponent.name == addComponent) {
                        newAddComponent = cloneDeep(elem);
                    }
                    return;
                }
                    );
            newAddComponent.id = nanoid();
            newAddComponent.editorProp.position.top = top  + "px";
            newAddComponent.editorProp.position.left = left  + "px";
            newAddComponent.editorProp.size.width = width  + "px";
            newAddComponent.editorProp.size.height = height  + "px";
            tempArr.push(newAddComponent);
            setArrElem([...tempArr]);
        setHid("hidden");
        setFlagAdd(false);
        setTop(0);
        setLeft(0);
        setWidth(0);
        setHeight(0);
    }
    };

    function    setSettings (sett, id) {
        if (id === "top") {
            setTop(sett);
        };
        if (id === "left") {
            setLeft(sett);
        };
        if (id === "width") {
            setWidth(sett);
        };
        if (id === "height") {
            setHeight(sett);
        };
        if (id === "fontSize") {
            let tempObj = {...templateProp};
            tempObj.formatData.fontSize = sett + "px";
            setTemplateProp(tempObj)
        };

    };

    function setProp(id, prop, x, y) {
        let index = getPos(id);
        let tempArr = [...arrElem];
        if (prop === "position") {
            tempArr[index].editorProp[`${prop}`].top = y;
            tempArr[index].editorProp[`${prop}`].left = x;
        };
        if (prop === "size") {
            tempArr[index].editorProp[`${prop}`].width = x;
            tempArr[index].editorProp[`${prop}`].height = y;
        };
        setArrElem([...tempArr]);
    };

    function delElem(id) {
        setSelect(false);
        let index = getPos(id);
        let tempArr = [...arrElem];
        tempArr.splice(index,1);
        setArrElem([...tempArr]);
    };

    function getPos(id) {
        let arr2 =arrElem.map((elem,i)=>elem.id);//преобразование массива в массив id
        return arr2.indexOf(id);//позиция id в массиве
    };

    function dragOverHandler(e) {
        e.preventDefault();
    };

    function paintElem(top, left, height, width) {
        setTop(top);
        setLeft(left);
        setHeight(height);
        setWidth(width);
        setHid("");
    };

    function selectElem(id) {
        console.log("id", id);
        let index = getPos(id);
        let tempArr = [...arrElem];
        console.log("tempArr[index].editorProp.select", tempArr[index].editorProp.select);
        tempArr.map(elem => elem.editorProp.select = false);
        tempArr[index].editorProp.select = true;
        setArrElem([...tempArr]);
        setHid("");
        setTop(eval((tempArr[index].editorProp.position.top).slice(0, -2)));
        setLeft(eval((tempArr[index].editorProp.position.left).slice(0, -2)));
        setHeight(eval((tempArr[index].editorProp.size.height).slice(0, -2)));
        setWidth(eval((tempArr[index].editorProp.size.width).slice(0, -2)));
        setId(id);
        setViewSettingsComponent(tempArr[index].viewSettingsComponentName);
        setTemplateProp ({...tempArr[index].templateProp});
        setSelect(true);
    };

    let arr2 =[...props.arrElemDefault];//массив имен для кнопок
    let btn = arr2.map((elem)=>(
        elem.viewComponent.name));

    let viewSettingsComponent2;//переделать без условий, сделать с вызовом компонента

    if (viewSettingsComponent == undefined) {
        viewSettingsComponent2 = SettingsComponent;
       }
       else{
           if (viewSettingsComponent == "textAreaViewSettingsComponent") {
               viewSettingsComponent2 = TextAreaViewSettingsComponent;
           };
           if (viewSettingsComponent == "textViewSettingsComponent") {
               viewSettingsComponent2 = TextViewSettingsComponent;
           };
           if (viewSettingsComponent == "tableViewSettingsComponent") {
               viewSettingsComponent2 = TableViewSettingsComponent;
           };
           console.log("ViewSettingsComponent", viewSettingsComponent2);
       };

    function onButtonClick(name) {
        console.log("name button component", name);
        setFlagAdd(true);
        setHid("");
        setAddComponent(name);
    };

//=============================================style============================================================================
    const styleEditor = {
        outline: "2px solid red",
        //size a4
        // height: "29.7cm",
        // width: "21cm",
        height: pageSizeHeight,
        width: pageSizeWidth,
        display: "inline-block",
        marginLeft: "400px",
        marginTop: "50px",
        position: "absolute",
    };
    const pageMargins = {
        outline: "1px dotted grey",
        height: pageMarginsSizeHeight,
        width: pageMarginsSizeWidth,
        //размер а4
        // height: "1119px",
        // width: "790px",
        position: "absolute",
        marginLeft: pageMarginsLeft,
        marginTop: pageMarginsTop,
        marginRight: pageMarginsRight,
        marginBottom: pageMarginsBottom,
    };
    const styleHeader = {
        border: "2px solid red",
        height: "50px",
        display: "inline-block",
    };
    const styleFooter = {
        border: "2px solid red",
        height: "50px",
        display: "inline-block",
        marginTop: "900px",
    };
    const styleArrow = {
        width: "16px",
        height: "16px",
        position: "absolute",
        bottom: "0px",
        right: "0px",
    };
    const styleTempElem = {
        top: top + "px",
        left: left + "px",
        height: height + "px",
        width: width + "px",
        outline: "1px dotted red",
        position: "absolute",
        visibility: `${hid}`,
    };
    // const styleCircle1 = {
    //     top: "-9px",
    //     left: width/2-10 + "px",
    //     color: "red",
    //     position: "absolute",
    //     zIndex: "1000",
    // };
    //
    // const styleCircle2 = {
    //     top: height/2-11 +"px",
    //     left: -9 + "px",
    //     color: "red",
    //     position: "absolute",
    //     zIndex: "1000",
    // };
    //
    // const styleCircle3 = {
    //     top: height/2-11 +"px",
    //     left: width-11 + "px",
    //     color: "red",
    //     position: "absolute",
    //     zIndex: "1000",
    // };
    //
    // const styleCircle4 = {
    //     top: height-11 +"px",
    //     left: width/2-10 + "px",
    //     color: "red",
    //     position: "absolute",
    //     zIndex: "1000",
    // };
//=============================================style============================================================================

    return (
        <div>
            <EditorToolbarComponent
                copyParamToPdf={props.copyParamToPdf}
                createReport={props.createReport}
                arrElem={arrElem}
                btn={btn}
                delElem = {delElem}
                id = {id}
                select={select}
                arrElemDefault={props.arrElemDefault}
                onButtonClick={onButtonClick}
                pageMarginsLeft={pageMarginsLeft}
                pageMarginsTop={pageMarginsTop}
                pageMarginsRight={pageMarginsRight}
                pageMarginsBottom={pageMarginsBottom}
                pageOrientation={pageOrientation}
                pageSize={pageSize}
            />

            <div
                onDragOver={(e) => dragOverHandler (e)}
                style={styleEditor}
            >
                <HeaderComponent
                    pageMarginsSizeHeight={pageMarginsSizeHeight}
                    pageMarginsSizeWidth={pageMarginsSizeWidth}
                    pageMarginsLeft={pageMarginsLeft}
                    pageMarginsRight={pageMarginsRight}
                />
                <div
                //рамка отступы
                    style={pageMargins}
                    onMouseDown={mouseDownHandlerNewElement}
                    onMouseUp={mouseUpHandlerNewElement}
                    onMouseMove={mouseMoveHandlerNewElement}
                >
                </div>
                <FooterComponent
                    pageMarginsSizeHeight={pageMarginsSizeHeight}
                    pageMarginsSizeWidth={pageMarginsSizeWidth}
                    pageMarginsLeft={pageMarginsLeft}
                    pageMarginsRight={pageMarginsRight}
                />
                {arrElem.map((elem,i)=>{
                        const EnhancedComponent = ResizeMoveWrapper(elem.viewComponent);
                        return <EnhancedComponent
                            templateProp = {elem.templateProp}
                            editorProp = {elem.editorProp}
                            id={elem.id}
                            getPos = {getPos}
                            delElem = {delElem}
                            setProp={setProp}
                            paintElem={paintElem}
                            selectElem={selectElem}
                            handleChangeH={handleChangeH}
                            setHInArr={setHInArr}
                            refH={refH}
                        />
                    }
                )}

                <div className="tempelement"
                     style={styleTempElem}
                     onMouseMove={mouseMoveHandlerNewElement}
                     onMouseUp={mouseUpHandlerNewElement}
                     mouseDownHandlerNewElement={mouseDownHandlerNewElement}
                >
                    {/*<i*/}
                    {/*    id="circle1"*/}
                    {/*    // onMouseDown={event => mouseDownHandler(event)}*/}
                    {/*    // onMouseMove={event => mouseMoveHandler(event)}*/}
                    {/*    // onMouseUp={event => mouseUpHandler(event)}*/}
                    {/*    style={styleCircle1}*/}

                    {/*    className="fas fa-circle"></i>*/}
                    {/*<i*/}
                    {/*    id="circle2"*/}
                    {/*    // onMouseDown={event => mouseDownHandler(event)}*/}
                    {/*    // onMouseMove={event => mouseMoveHandler(event)}*/}
                    {/*    // onMouseUp={event => mouseUpHandler(event)}*/}
                    {/*    style={styleCircle2}*/}
                    {/*    className="fas fa-circle"></i>*/}
                    {/*<i*/}
                    {/*    id="circle3"*/}
                    {/*    // onMouseDown={event => mouseDownHandler(event)}*/}
                    {/*    // onMouseMove={event => mouseMoveHandler(event)}*/}
                    {/*    // onMouseUp={event => mouseUpHandler(event)}*/}
                    {/*    style={styleCircle3}*/}
                    {/*    className="fas fa-circle"></i>*/}
                    {/*<i*/}
                    {/*    id="circle4"*/}
                    {/*    // onMouseDown={event => mouseDownHandler(event)}*/}
                    {/*    // onMouseMove={event => mouseMoveHandler(event)}*/}
                    {/*    // onMouseUp={event => mouseUpHandler(event)}*/}
                    {/*    style={styleCircle4}*/}
                    {/*    className="fas fa-circle"></i>*/}
                </div>

            </div>

            <EditorViewSettingsSidebarComponent
                id={id}
                top={top}
                left={left}
                width={width}
                height={height}
                setProp={setProp}
                setSettings={setSettings}
                viewSettingsComponent={viewSettingsComponent2}
                templateProp={templateProp}
                pageMarginsLeft={pageMarginsLeft}
                pageMarginsTop={pageMarginsTop}
                pageMarginsRight={pageMarginsRight}
                pageMarginsBottom={pageMarginsBottom}
                reSizePageMarginsHandleChange={reSizePageMarginsHandleChange}
                reOrientation={reOrientation}
            />
        </div>
    );
};

export default EditorTemplateViewAreaComponent;